.include "m16def.inc"

.def timer1 = r16
.def timer2 = r17
.def timer3 = r18
.def beepCounter = r19
.def pressCounter = r20
.def temp = r21
.def beepBtn = r22
.def maxPress = r23
.def beepDelay = r24
.def nextBeep = r25

; PORTA[0..7] : Keypad
; PORTB[0..7] : DATA LCD
; PORTC[0..7] : LED
; PORTD[0..1] : Difficulty LED
; PORTD[2..3] : Button
; PORTD[5] : EN LCD
; PORTD[6] : R/W LCD
; PORTD[7] : RS LCD
; INT0: PORTD bit 2
; INT1: PORTD bit 3
.org $00
rjmp INIT
.org $02
rjmp RUN
.org $04
rjmp CHANGE_DIFF

INIT:
	ser temp
	out DDRA, temp
	out DDRB, temp
	out PORTC, temp
	; Init keypad
	ldi temp, 0b01110000
	out DDRA, temp
	ldi temp, 0b00001111
	out PORTA, temp

	ldi temp, 1
	mov R10, temp
	out PORTD, R10

INIT_Z_SPH:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_INTERRUPT:
	ldi temp,0b00001010 ; Triggers INT0 and INT1 at falling edge
	out MCUCR,temp
	ldi temp,0b11000000
	out GICR,temp
	sei

IDLE:
	ser temp
	out PORTC, temp
	rjmp IDLE

CHANGE_DIFF:
	MOV temp, R10
	CPI temp, 1
	BREQ SET_HARD
	LSR R10
	RJMP UPD_DIFF_LED
	SET_HARD:
	LSL R10

	UPD_DIFF_LED:
	; Update Difficulty LED
	OUT PORTD, R10
	RETI

EXEC_LCD:
	sbi PORTD, 5	; Set EN
	CBI PORTD, 5	; Clear EN
	ret

MSG_MODE:
	ldi temp, 0x06	; Decrease cursor, display scroll on
	out PORTB, temp
	rcall EXEC_LCD
	ret

HISTORY_MODE:
	ldi temp, 0x05	; Decrease cursor, scroll on
	out PORTB, temp
	rcall EXEC_LCD
	ret

INIT_LCD:
	cbi PORTD, 7	; Clear RS
	ldi temp, 0x30	; 8bit, 1line, 5x7
	out PORTB, temp
	rcall EXEC_LCD
	cbi PORTD, 7
	ldi temp, 0x0E	; display ON, cursor ON, blink OFF
	out PORTB, temp
	rcall EXEC_LCD
	ret
	
CLEAR_LCD:
	cbi PORTD, 7; CLR RS
	ldi temp,0x01 ; MOV DATA,0x01
	out PORTB,temp
	rcall EXEC_LCD
	ret

KEY_0:
	LDI temp, 0
	RET
KEY_1:
	LDI temp, 1
	RET
KEY_2:
	LDI temp, 2
	RET
KEY_3:
	LDI temp, 3
	RET
KEY_4:
	LDI temp, 4
	RET
KEY_5:
	LDI temp, 5
	RET
KEY_6:
	LDI temp, 6
	RET
KEY_7:
	LDI temp, 7
	RET
KEY_8:
	LDI temp, 8
	RET
KEY_9:
	LDI temp, 9
	RET

DECIPHER_KEY:
	; Assume that temp contains row and col of pressed key
	CPI temp, 0b00010001
	BREQ KEY_1
	CPI temp, 0b00100001
	BREQ KEY_2
	CPI temp, 0b01000001
	BREQ KEY_3
	CPI temp, 0b00010010
	BREQ KEY_4
	CPI temp, 0b00100010
	BREQ KEY_5
	CPI temp, 0b01000010
	BREQ KEY_6
	CPI temp, 0b00010100
	BREQ KEY_7
	CPI temp, 0b00100100
	BREQ KEY_8
	CPI temp, 0b01000100
	BREQ KEY_9
	CPI temp, 0b00101000
	BREQ KEY_0
	; Key is invalid
	LDI temp, 10
	RET


WRITE_DIGIT:
	sbi PORTD, 7
	SUBI temp, -0x30
	out PORTB, temp
	SUBI temp, 0x30
	rcall EXEC_LCD
	ret

WRITE_STAR:
	sbi PORTD, 7
	ldi temp, 42
	out PORTB, temp
	rcall EXEC_LCD
	ret
	
CHECK_KEYPAD:
	; Power Up Row
	LDI temp, 0x0F
	OUT DDRA, temp
	OUT PORTA, temp
	LDI temp, 0xF0
	; Read Column
	IN R14, PINA
	; Power Up Column
	OUT DDRA, temp
	OUT PORTA, temp
	; Read Row
	NOP
	IN R15, PINA
	AND R14, R15
	; Now temp contains coordinate of pressed key
	MOV temp, R14

	CPI temp, 0
	BREQ CHECK_LAST_KEY; Process last key pressed
	; Save this as last key pressed
	; and process after key is released
	MOV R13, temp
	RET

	CHECK_LAST_KEY:
	CP temp, R13
	BRNE PRE_PRESS
	RET
	PRE_PRESS:
	MOV temp, R13
	RCALL DECIPHER_KEY
	CLR R13
	CPI temp, 10
	BRNE CONFIRM_KEY
	RET
	CONFIRM_KEY:
	RCALL ON_PRESS
	RET

RUN:
	; Resets timer and counter
	LDI temp, 0
	OUT PORTC, temp
	LDI timer1, 0x00
	LDI timer2, 0x00
	MOV temp, R10
	CPI temp, 1
	BREQ TIMER_EASY
	LDI timer3, 3 ; Hard: 3 (5min 52sec)
	LDI maxPress, 100
	RJMP SKIP_TIMER_EASY
	TIMER_EASY:
	LDI timer3, 6 ; Easy: 6 (11min 45sec) (1 cycle = 68/150
	LDI maxPress, 50
	SKIP_TIMER_EASY:

	LDI beepCounter, 0
	LDI pressCounter, 0

	; Determine beep button and maximum number of press allowed
	LDI beepBtn, 0
	LDI beepDelay, 2
	LDI nextBeep, 0xFF

	; Set up beep queue
	LDI XL, low(0x60)
	LDI XH, high(0x00)
	LDI YL, low(0x60)
	LDI YH, high(0x00)

	; Clear LCD and set cursor on right position
	rcall CLEAR_LCD
	rcall HISTORY_MODE
	
	DEC_0:
	
	; Check if a button is pressed and call ON_PRESS if so
	RCALL CHECK_KEYPAD
	CPI pressCounter, 0xFE
	BREQ END_RUN
	CPI pressCounter, 0xFF
	BREQ END_RUN

	DEC timer1
	BRNE DEC_0
	DEC timer2
	BRNE DEC_0
	DEC timer3
	BRNE DEC_0

	RCALL LOSE
	END_RUN:
	RETI

ON_PRESS:
	; 1. Update LCD
	; 2. If the button pressed is the right one:
	;     b. Turn on beep LED
	;     c. Schedule the next beep
	; 3. If a beep should show up now:
	;     a. Update the Beep LED
	;     b. Increment beepCounter
	; 4. Increment pressCounter
	; 5. If the beepCounter reaches 10, player wins
	; 6. If pressCounter reaches max, player loses

	; Get the pressed number via temp

	RCALL WRITE_DIGIT	

	CP temp, beepBtn
	BRNE SKIP_DELAY_SET        ; If the button presses is the right one...
	MOV nextBeep, pressCounter      ; Schedule the next beep (Push to queue)
	ADD nextBeep, beepDelay
	ST Y+, nextBeep
	SKIP_DELAY_SET:
	
	CP XL, YL
	BREQ SKIP_UPDATE_BEEP
	LD nextBeep, X
	CP nextBeep, pressCounter
	BRNE SKIP_UPDATE_BEEP      ; If the nextBeep should show here...

	LD nextBeep, X+
	LSL beepCounter                ; Increment beepCounter
	INC beepCounter
	RCALL WRITE_STAR
	OUT PORTC, beepCounter

	SKIP_UPDATE_BEEP:
	INC pressCounter           ; Increment pressCounter
	CPI beepCounter, 0xFF        ; beepCounter reaches 8, so player wins
	BREQ WIN
	CP pressCounter, maxPress  ; Reaches max press limit, player loses
	BREQ LOSE ; BOOM
	RET

WRITE_MSG:
	lpm
	tst R0
	breq END_MSG
	sbi PORTD, 7
	out PORTB, R0
	rcall EXEC_LCD
	adiw ZL, 1
	rjmp WRITE_MSG

	END_MSG:
	ret

WIN:
	; Give heartwarming message
	rcall CLEAR_LCD
	rcall MSG_MODE
	ldi ZL, low(2*win_msg)
	ldi ZH, high(2*win_msg)
	rcall WRITE_MSG
	ldi pressCounter, 0xFE
	ret

LOSE:
	; Tell the players that they suck
	rcall CLEAR_LCD
	rcall MSG_MODE
	ldi ZL, low(2*lose_msg)
	ldi ZH, high(2*lose_msg)
	rcall WRITE_MSG
	ldi pressCounter, 0xFF
	ret

win_msg:
.db "Nice :D", 0
lose_msg:
.db "BOOM!!", 0
beep_queue:
.db 0
