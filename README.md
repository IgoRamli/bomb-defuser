# Bomb Defuser

**Deskripsi**

Tugas akhir POK dari kelompok "Semoga Langgeng":
*  Inigo Ramli
*  Mushaffa Huda
*  Exacta Febrinanto Abdillah

**Spesifikasi Hardware**

Proyek ini menggunakan AtMega 16 dengan jumlah port sebanyak 4 (Port A, B, C, D)

Posisi interrupt:
1.  INT0    : PORTD[2]
2.  INT1    : PORTD[3]
3.  INT2    : PORTB[2]

Komponen AVR yang digunakan beserta port yang digunakan:
1.  Keypad  (PORTA[0..7])
2.  LCD
    - Data  (PORTB[0..7])
    - EN    (PORTD[5])
    - R/W   (PORTD[6])
    - RS    (PORTD[7])
3.  LED     (PORTC[0..7])
4.  Button
    - Start     (PORTD[2])
    - Dificulty (PORTD[3])
    
**Petunjuk Penggunaan**

Bomb Defuser adalah sebuah program AVR yang menguji kemampuan deduksi dan mencari pola.
    
Pada awal permainan, Anda diberikan sebuah 'bom' yang terdiri atas *keypad*, papan LCD, dan delapan buah LED. Tugas Anda adalah menjinakkan bom tersebut! Agar bom dapat dijinakkan, Anda harus menekan sebuah tombol rahasia (Sebut saja tombol X) di keypad sebanyak 8 kali. Ketika tombol X ditekan, maka satu lampu LED akan menyala setelah Anda menekan sejumlah tombol lainnya. Jumlah tombol yang harus ditekan sebelum LED akan menyala selalu konstan (Sebut saja sebanyak Y). Jika semua lampu LED telah menyala, maka Anda telah berhasil menjinakkan bom! Namun, jika Anda telah menekan terlalu banyak tombol, atau permainan telah berjalan selama lebih dari beberapa menit, maka bom akan meledak dan Anda dinyatakan kalah.

Terdapat dua tingkat kesulitan pada permainan ini: Tingkat kesulitan *Easy* dan *Hard*. Pada tingkat *Hard*, batas waktu permainan dan jumlah penekanan tombol lebih sedikit dari tingkat *Easy*.

Untuk memudahkan penjinakkan, terdapat sebuah panel LCD yang Akan menampilkan daftar 16 tombol terakhir yang Anda tekan. Apabila pada saat Anda menekan suatu tombol, ada lampu LED yang menyala, maka akan muncul tanda asteriks (\*) di sebelah kiri angka tombol tersebut. Sebagai contoh: Apabila pada panel LCD terdapat daftar "527\*890\*39", maka Sejak awal permainan, Anda telah menekan tombol 9, 3, 0, 9, 8, 7, 2, 5. Selain itu, pada saat Anda menekan tombol 8 dan 3, ada lampu LED yang menyala.